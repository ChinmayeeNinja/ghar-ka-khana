import React from "react";

import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./common/pages/Login";
import Signup from "./common/pages/Signup";
import {
  VENDOR_URL,
  CONSUMER_URL,
  LOGIN_URL,
  SIGNUP_URL,
} from "./common/utils/urlConstants";
import Vendor from "./vendor";
import Consumer from "./consumer";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path={LOGIN_URL} component={Login} />
        <Route path={SIGNUP_URL} component={Signup} />
        <Route path={VENDOR_URL} component={Vendor} />
        <Route path={CONSUMER_URL} component={Consumer} />
      </Switch>
    </Router>
  );
}

export default App;
