import { userDetails } from "./userDetails";
import { orderDetails } from "./orderDetails";
import { dishesDetails } from "./dishesDetails";

const createModels = () => ({
    userDetails: userDetails,
    orderDetails: orderDetails,
    dishesDetails: dishesDetails,
})

export default createModels