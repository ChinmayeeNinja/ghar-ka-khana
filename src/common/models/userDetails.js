import { USER_TYPE } from "../utils/userConstants"

export const userDetailsInitialState = {
    userProfile: null,
    userType: USER_TYPE.CHEF,
}

export const userDetails = {
    state: userDetailsInitialState,
    reducers: {
        // handle state changes with pure functions
        setUserProfile(state, payload) {
            return { ...state, userProfile: payload }
        },
        setUserType(state, payload) {
            return { ...state, userType: payload }
        },
    },
    effects: dispatch => ({
        // handle state changes with impure functions.
        // use async/await for async actions
        async incrementAsync(payload, rootState) {
            await new Promise(resolve => setTimeout(resolve, 1000))
            dispatch.userDetails.setUsername(payload)
        },
    }),
}