import { getAllDishesByChef } from "../../vendor/api/dishes";
export const dishesDetailsInitialState = {
  dishListByChef: [],
  dishCategoryList: [],
  selectedDish: "",
  selectedDishDetails: [],
};

export const dishesDetails = {
  state: dishesDetailsInitialState,
  reducers: {
    saveDishesList(state, payload) {
      return { ...state, dishListByChef: payload };
    },
    saveDishesCategoryList(state, payload) {
      return { ...state, dishCategoryList: [...payload] };
    },
    saveNewItem(state, payload) {
      console.log(payload.itemImage);
      const newItem = {
        name: payload.itemName,
        description: payload.itemDesc,
        photo: payload.itemImage,
        base_price: payload.itemPrice,
        final_price: Number(payload.itemPrice) + 100,
        status: "ACTIVE",
        type: payload.itemType,
        category: payload.itemCategory,
        quantity: payload.itemQuantity,
      };
      const newDishList = [...state.dishListByChef, newItem];
      return { ...state, dishListByChef: newDishList };
    },
    selectDish(state, payload) {
      return { ...state, selectedDish: payload };
    },
    saveDishDetails(state, payload) {
      return { ...state, selectedDishDetails: payload };
    },
    updateItem(state, payload) {
      console.log(payload.itemImage);
      const update = {
        name: payload.itemName,
        description: payload.itemDesc,
        photo: payload.itemImage,
        base_price: payload.itemPrice,
        final_price: Number(payload.itemPrice) + 100,
        status: "ACTIVE",
        type: payload.itemType,
        category: payload.itemCategory,
        quantity: payload.itemQuantity,
      };
      const dishIndex = state.dishListByChef.findIndex(
        (element) => element.id === state.selectedDish
      );
      const newArray = [...state.dishListByChef];
      newArray[dishIndex] = { ...newArray[dishIndex], ...update };
      return { ...state, dishListByChef: newArray };
    },
    getDishDetails(state, payload) {
      const newArray = [...state.dishListByChef];
      const selectedDish = newArray.filter((item) => item.id === payload);
      return { ...state, selectedDishDetails: selectedDish };
    },
  },
  effects: (dispatch) => ({
    async getAllDishesForChef(payload) {
      getAllDishesByChef().then(async (response) => {
        await dispatch.dishesDetails.saveDishesList(response.dishes);
        const catList = new Set(response.dishes.map((dish) => dish.category));
        dispatch.dishesDetails.saveDishesCategoryList(catList);
      });
    },
    async saveNewItem(payload) {
      // TODO: make API call to add a new item to DB.
      // Once added we get the full list for the menu page as response or make call again
      // This is because we cannot store images in redux store easily.
    },
    async getDishDetailss(payload) {
      // getAllDishesByChef().then(async (response) => {
      //   await dispatch.dishesDetails.saveDishDetails(
      //     response.dishes.filter((item) => item.id === payload)
      //   );
      // });
    },
  }),
};
