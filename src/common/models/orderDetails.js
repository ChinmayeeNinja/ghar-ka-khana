import { getAllOrders } from '../../vendor/api/orders';
export const orderDetailsInitialState = {
    ordersList: {
        incomingOrders: [],
        acceptedOrders: [],
        deliveryOrders: [],
        completedOrders: [],
        cancelledOrders: [],
    }
}

export const orderDetails = {
    state: orderDetailsInitialState,
    reducers: {
        // handle state changes with pure functions
        saveOrdersList(state, payload) {
            let incomingOrders = [];
            let acceptedOrders = [];
            let deliveryOrders = [];
            let completedOrders = [];
            let cancelledOrders = [];
            payload.map(order => {
                switch (order.status) {
                    case ('RAISED'):
                        incomingOrders.push(order);
                        break;
                    case ('ACCEPTED'):
                        acceptedOrders.push(order);
                        break;
                    case ('FOR-DELIVERY'):
                        deliveryOrders.push(order);
                        break;
                    case ('COMPLETED'):
                        completedOrders.push(order);
                        break;
                    case ('CANCELLED'):
                        cancelledOrders.push(order);
                        break;
                }
            });
            return { ...state, ordersList: { incomingOrders, acceptedOrders, deliveryOrders, completedOrders, cancelledOrders } }
        },
    },
    effects: dispatch => ({
        // handle state changes with impure functions.
        // use async/await for async actions
        async getOrdersList(payload) {
            getAllOrders().then(response =>
                dispatch.orderDetails.saveOrdersList(response.orders)
            )
        },
    }),
}