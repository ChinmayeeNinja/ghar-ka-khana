import React from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import { GoogleLogin } from "react-google-login";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import FastfoodIcon from "@material-ui/icons/Fastfood";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import KitchenIcon from "@material-ui/icons/Kitchen";
import { useSelector } from "react-redux";
import { VENDOR_URL, CONSUMER_URL } from "../utils/urlConstants";
import { USER_TYPE } from "../utils/userConstants";
import { SIGNUP_URL } from "../utils/urlConstants";
const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "left",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  toggleBtnSelected: {},
  toggleBtnUnselected: {},
  googleSignin: {
    width: "100%",
  },
}));

export default function Login(props) {
  console.log(props);
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const userTypeSelection = useSelector((state) => state.userDetails.userType);
  const setUserTypeSelection = (name) => {
    dispatch.userDetails.setUserType(name);
  };

  const successGoogleCallback = (response) => {
    console.log(response);
    sessionStorage.setItem("userData", JSON.stringify(response));
    dispatch.userDetails.setUserProfile(response.profileObj);
    if (userTypeSelection === USER_TYPE.CHEF) history.push(VENDOR_URL);
    else history.push(CONSUMER_URL);
  };

  const failureGoogleCallback = (response) => {
    console.log(response);
  };
  const handleclick = () => {
    props.history.push(SIGNUP_URL);
  };
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Welcome!
        </Typography>
        <Typography variant="subtitle1">Login to your account.</Typography>
        <br />
        <ToggleButtonGroup value="left">
          <ToggleButton
            value="bold"
            selected={userTypeSelection === USER_TYPE.FOODIE}
            className={
              userTypeSelection === USER_TYPE.FOODIE
                ? classes.toggleBtnSelected
                : classes.toggleBtnUnselected
            }
            onClick={() => setUserTypeSelection(USER_TYPE.FOODIE)}
          >
            <FastfoodIcon /> &nbsp;I'm a Foodie
          </ToggleButton>
          <ToggleButton
            value="bold"
            selected={userTypeSelection === USER_TYPE.CHEF}
            className={
              userTypeSelection === USER_TYPE.CHEF
                ? classes.toggleBtnSelected
                : classes.toggleBtnUnselected
            }
            onClick={() => setUserTypeSelection(USER_TYPE.CHEF)}
          >
            <KitchenIcon /> &nbsp;I'm a Chef
          </ToggleButton>
        </ToggleButtonGroup>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <br />
          <center>OR</center>
          <Box mt={1}>
            <GoogleLogin
              clientId="590511010733-mikun98utkjnm12dbpjj24d2ss70vmec.apps.googleusercontent.com"
              onSuccess={successGoogleCallback}
              onFailure={failureGoogleCallback}
              cookiePolicy={"single_host_origin"}
              className={classes.googleSignin}
            />
          </Box>
          <br />
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item onClick={handleclick}>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}
