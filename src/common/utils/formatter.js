import { format } from "date-fns";

export const dateFormatter = (date) => {
    var result = format(new Date(Number(date)), 'HH:MM a')
    return result
}