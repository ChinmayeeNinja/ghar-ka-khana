//Common URLs
export const DASHBOARD_URL = '/dashboard'
export const SIGNUP_URL = '/signup'
export const LOGIN_URL = '/login'
export const VENDOR_URL = '/vendor'
export const CONSUMER_URL = '/consumer'