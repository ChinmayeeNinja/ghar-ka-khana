import { init } from '@rematch/core'
import createModels from './common/models';

const configureStore = () => {
    const models = createModels();

    const store = init({
        models,
    })

    return store;
}

export default configureStore;