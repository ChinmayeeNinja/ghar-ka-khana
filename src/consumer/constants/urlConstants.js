// Foodie URLs
export const EXPLORE_URL = '/explore'
export const SEARCH_URL = '/search'
export const CART_URL = '/cart'
export const FOODIE_ACCOUNT_URL = '/account'