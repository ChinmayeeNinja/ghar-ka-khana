import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { EXPLORE_URL, SEARCH_URL, CART_URL, FOODIE_ACCOUNT_URL } from './constants/urlConstants';
import Explore from './pages/Explore';
import Search from './pages/Search';
import Cart from './pages/Cart';
import FoodieProfile from './pages/FoodieProfile';

function Consumer({ match }) {
    return (
        <>
            {/* <Route path={`${match.path}`} exact>
                <Explore />
            </Route>
            <Route path={`${match.path}${EXPLORE_URL}`}>
                <Explore />
            </Route>
            <Route path={`${match.path}${SEARCH_URL}`}>
                <Search />
            </Route>
            <Route path={`${match.path}${CART_URL}`}>
                <Cart />
            </Route>
            <Route path={`${match.path}${FOODIE_ACCOUNT_URL}`}>
                <FoodieProfile />
            </Route> */}
        </>
    );
}

export default Consumer;
