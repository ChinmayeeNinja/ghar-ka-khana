import React, { useEffect } from 'react';
import { useSelector } from "react-redux";
import { useHistory } from 'react-router-dom';
import NavigationBar from '../components/NavigationBar';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Dishes from '../components/Dishes/Dishes';

export default function Explore() {
    const userProfile = useSelector(state => state.userDetails.userProfile);
    const userType = useSelector(state => state.userDetails.userType);
    const history = useHistory();

    useEffect(() => {
        const data = JSON.parse(sessionStorage.getItem('userData'));
        if (!data) {
            history.push('/')
        } else {
            // TODO: call backend API to verify the id token https://developers.google.com/identity/sign-in/web/backend-auth
        }
    })
    return (
        <>
            <div>Hey {userProfile ?.givenName || 'Bro'}!</div>
            <div></div>
            <NavigationBar userType={userType} />
            <Dishes />
        </>
    )
}