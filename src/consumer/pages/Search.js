import React from 'react';
import { useSelector } from "react-redux";
import NavigationBar from '../components/NavigationBar';

export default function Search() {
    const userType = useSelector(state => state.userDetails.userType);

    return (
        <>
            <NavigationBar userType={userType} />
        </>
    )
}