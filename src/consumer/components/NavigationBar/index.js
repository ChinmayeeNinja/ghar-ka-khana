import React from 'react';
import FoodieNavigationBar from "./FoodieNavigationBar";
import { makeStyles } from '@material-ui/core/styles';

export default function NavigationBar({ userType }) {
    const classes = useStyles();
    
    return (
        <div className={classes.footer}>
            <FoodieNavigationBar />
        </div>
    )
}

const useStyles = makeStyles((theme) => ({
    footer: {
        position: "fixed",
        bottom: 0,
        width: "105%",
        left: '-5%'
    }
}));