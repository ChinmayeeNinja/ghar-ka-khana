import React from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import SearchIcon from '@material-ui/icons/Search';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import { EXPLORE_URL, SEARCH_URL, CART_URL, FOODIE_ACCOUNT_URL } from '../../constants/urlConstants';
import { useHistory } from 'react-router-dom';
import {getKeyByValue} from '../../../common/utils/objectUtility';
import { CONSUMER_URL } from '../../../common/utils/urlConstants';

export const FoodieNavigationBar = () => {
    const history = useHistory();
    return (
        <BottomNavigation
            value={parseInt(getKeyByValue(ROUTE_OPTIONS, history.location.pathname))}
            onChange={(event, newValue) => {
                history.push(ROUTE_OPTIONS[newValue])
            }}
            showLabels
        >
            <BottomNavigationAction label="Explore" icon={<FavoriteBorderIcon />} />
            <BottomNavigationAction label="Search" icon={<SearchIcon />} />
            <BottomNavigationAction label="Cart" icon={<ShoppingCartOutlinedIcon />} />
            <BottomNavigationAction label="Account" icon={<PersonOutlineIcon />} />
        </BottomNavigation>
    )
}
const ROUTE_OPTIONS = {
    0: CONSUMER_URL+EXPLORE_URL,
    1: CONSUMER_URL+SEARCH_URL,
    2: CONSUMER_URL+CART_URL,
    3: CONSUMER_URL+FOODIE_ACCOUNT_URL,
}

export default FoodieNavigationBar;