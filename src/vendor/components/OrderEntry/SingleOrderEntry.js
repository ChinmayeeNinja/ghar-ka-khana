import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';

const useStyles = makeStyles((theme) => ({
    inline: {
        display: 'inline',
    },
    alignRight: {
        float: 'right',
    },
}));
export default function SingleOrderEntry({ order }) {
    const classes = useStyles();
    return (
        <>
            <ListItem alignItems="flex-start">
                <ListItemText
                    primary={
                        <>
                        #{order.orderId}
                        <span className={classes.alignRight}>
                        {order.status}
                        </span>
                        </>
                    }
                    secondary={
                        <>
                            <div>
                                {order.dish_name}
                            </div><div>
                                <Typography
                                    component="span"
                                    variant="body2"
                                    className={classes.inline}
                                    color="textPrimary"
                                >
                                    ₹{order.order_price}
                                </Typography>
                            </div>
                        </>
                    }
                />
            </ListItem>
            <Divider component="li" />
        </>
    )
}