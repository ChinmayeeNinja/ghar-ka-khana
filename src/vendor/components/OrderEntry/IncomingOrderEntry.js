import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import { Button } from '@material-ui/core';
import { dateFormatter } from '../../../common/utils/formatter';

const useStyles = makeStyles((theme) => ({
    inline: {
        display: 'inline',
    },
    alignRight: {
        float: 'right',
    },
}));
export default function IncomingOrderEntry({ order }) {
    const classes = useStyles();
    return (
        <>
            <ListItem alignItems="flex-start">
                <ListItemText
                    primary={
                        <>
                            #{order.orderId} &nbsp;&nbsp; <Typography
                                    component="span"
                                    variant="body2"
                                    className={classes.inline}
                                    color="textSecondary"
                                >
                                    {dateFormatter(order.date)}
                                </Typography>
                            <span className={classes.alignRight}>
                                Incoming
                        </span>
                        </>
                    }
                    secondary={
                        <>
                            <div>
                                {order.dish_name}
                            </div><div>
                                <Typography
                                    component="span"
                                    variant="body2"
                                    className={classes.inline}
                                    color="textPrimary"
                                >
                                    ₹{order.order_price}
                                </Typography>
                            </div>
                            <div>
                                <span className={classes.alignRight}>
                                    <Button color="secondary">Decline</Button>
                                    <Button variant="contained" color="secondary">
                                        Confirm
                                    </Button>
                                </span>
                            </div>
                        </>
                    }
                />
            </ListItem>
            <Divider component="li" />
        </>
    )
}