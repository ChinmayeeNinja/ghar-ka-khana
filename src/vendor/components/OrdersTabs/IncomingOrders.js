import React from 'react';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import IncomingOrderEntry from '../OrderEntry/IncomingOrderEntry';

export default function IncomingOrders({ ordersList, classes }) {
    return (
        <>
            <List className={classes.root}>
                <Divider component="li" />
                {ordersList.incomingOrders.map(order =>
                    <div key={order.orderId}>
                        <IncomingOrderEntry order={order} />
                    </div>
                )}
            </List>
        </>
    )
}