import React from 'react';
import SingleOrderEntry from '../OrderEntry/SingleOrderEntry';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import IncomingOrderEntry from '../OrderEntry/IncomingOrderEntry';


export default function AllOrders({ ordersList, classes }) {
    return (
        <>
            <List className={classes.root}>
                <ListItem className={classes.listHeader} alignItems="flex-start">
                    Incoming
                </ListItem>
                <Divider component="li" />
                {ordersList.incomingOrders.map(order =>
                    <div key={order.orderId}>
                        <IncomingOrderEntry order={order} />
                    </div>
                )}
                <ListItem className={classes.listHeader} alignItems="flex-start">
                    Preparing
                </ListItem>
                <Divider component="li" />
                {ordersList.acceptedOrders.map(order =>
                    <div key={order.orderId}>
                            <SingleOrderEntry order={order} />
                    </div>
                )}
                {ordersList.deliveryOrders.map(order =>
                    <div key={order.orderId}>
                            <SingleOrderEntry order={order} />
                    </div>
                )}
                <ListItem className={classes.listHeader} alignItems="flex-start">
                    Declined
                </ListItem>
                <Divider component="li" />
                {ordersList.cancelledOrders.map(order =>
                    <div key={order.orderId}>
                            <SingleOrderEntry order={order} />
                    </div>
                )}
                <ListItem className={classes.listHeader} alignItems="flex-start">
                    Completed
                </ListItem>
                <Divider component="li" />
                {ordersList.completedOrders.map(order =>
                    <div key={order.orderId}>
                            <SingleOrderEntry order={order} />
                    </div>
                )}
            </List>
        </>
    )
}

// TODO: Array display can be optimised for performance gains when order list grows