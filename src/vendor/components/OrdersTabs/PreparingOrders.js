import React from 'react';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import SingleOrderEntry from '../OrderEntry/SingleOrderEntry';

export default function PreparingOrders({ ordersList, classes }) {
    return (
        <>
            <List className={classes.root}>
                <Divider component="li" />
                {ordersList.acceptedOrders.map(order =>
                    <div key={order.orderId}>
                        <SingleOrderEntry order={order} />
                    </div>
                )}
                {ordersList.deliveryOrders.map(order =>
                    <div key={order.orderId}>
                        <SingleOrderEntry order={order} />
                    </div>
                )}
            </List>
        </>
    )
}