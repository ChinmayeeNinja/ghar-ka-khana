import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
  itemPrice: {
    float: "right",
  },
  itemstatusactive: {
    color: "#155724",
    right: 2,
    top: 3,
    position: "absolute",
    border: "1px solid #d4eddb",
    padding: 3,
    fontSize: 8,
    fontWeight: "bold",
    background: "#c3e6ca",
  },
  itemstatusdelete: {
    color: "#721c24",
    right: 2,
    top: 3,
    position: "absolute",
    border: "1px solid #f5c6cb",
    padding: 3,
    fontSize: 8,
    fontWeight: "bold",
    background: "#f8d7da",
  },
  itemName: {},
});

const DishItem = (props) => {
  const classes = useStyles();
  const [addClicked, setAddClicked] = useState(false);
  const [quantity, setQuantity] = useState(0);
  const [itemName] = useState(props.item_name);
  const [itemPrice] = useState(props.item_price);
  const [itemImage] = useState(props.image_url);
  const [itemId] = useState(props.item_id);
  const [itemDescription] = useState(props.item_description);

  const addItemHandler = () => {
    props.onAddItem({
      itemId,
      itemName,
      itemPrice,
      quantity: 1,
      totalPrice: itemPrice,
    });
    console.log(quantity);
  };

  const reduceQuantityHandler = () => {
    console.log("reducer clicked");
    setQuantity(quantity - 1);
    console.log(quantity);
    props.onReduceQuantity(itemId, quantity - 1);
    if (quantity === 1) {
      props.onRemoveItem(itemId, itemPrice);
    }
  };

  const increaseQuantityHandler = () => {
    console.log("increase clicked");
    setQuantity(quantity + 1);
    console.log(quantity);
    props.onIncreseQuantity(itemId, quantity + 1);
  };

  return (
    <Card className={classes.root} onClick={props.onClick} id={props.item_id}>
      <CardActionArea>
        {props.item_status !== "ACTIVE" ? (
          <Typography
            variant="body2"
            component="span"
            className={classes.itemstatusactive}
          >
            Active
          </Typography>
        ) : (
          <Typography
            variant="body2"
            component="span"
            className={classes.itemstatusdelete}
          >
            Deleted
          </Typography>
        )}

        <CardMedia
          className={classes.media}
          image={itemImage}
          title={itemName}
          onClick={props.onClick}
          id={props.item_id}
        />
        <CardContent>
          <div>
            <Typography
              variant="body2"
              color="textPrimary"
              component="span"
              className={classes.itemName}
              onClick={props.onClick}
              id={props.item_id}
            >
              {itemName}
            </Typography>
            <Typography
              variant="body2"
              color="textPrimary"
              component="span"
              className={classes.itemPrice}
              onClick={props.onClick}
              id={props.item_id}
            >
              &#8377;{itemPrice}
            </Typography>
          </div>
          <Typography
            variant="caption"
            color="textSecondary"
            component="span"
            onClick={props.onClick}
            id={props.item_id}
          >
            {itemDescription}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default DishItem;
