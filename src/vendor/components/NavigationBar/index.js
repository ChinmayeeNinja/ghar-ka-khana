import React from 'react';
import ChefNavigationBar from "./ChefNavigationBar";
import { makeStyles } from '@material-ui/core/styles';

export default function NavigationBar({ userType }) {
    const classes = useStyles();

    return (
        <div className={classes.footer}>
            <ChefNavigationBar />
        </div>
    )
}

const useStyles = makeStyles((theme) => ({
    footer: {
        position: "fixed",
        bottom: 0,
        width: "105%",
        left: '-5%'
    }
}));