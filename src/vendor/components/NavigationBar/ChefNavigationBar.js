import React from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import RestaurantMenuOutlinedIcon from '@material-ui/icons/RestaurantMenuOutlined';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import SpeedOutlinedIcon from '@material-ui/icons/SpeedOutlined';
import { ORDERS_URL, MENU_URL, BUSINESS_URL, CHEF_ACCOUNT_URL } from '../../constants/urlConstants';
import { useHistory } from 'react-router-dom';
import {getKeyByValue} from '../../../common/utils/objectUtility';
import { VENDOR_URL } from '../../../common/utils/urlConstants';

export const ChefNavigationBar = () => {
    const history = useHistory();
    return (
        <BottomNavigation
        value={parseInt(getKeyByValue(ROUTE_OPTIONS, history.location.pathname))}
        onChange={(event, newValue) => {
            history.push(ROUTE_OPTIONS[newValue])
        }}
            showLabels
        >
            <BottomNavigationAction label="Orders" icon={<FavoriteBorderIcon />} />
            <BottomNavigationAction label="Menu" icon={<RestaurantMenuOutlinedIcon />} />
            <BottomNavigationAction label="Business" icon={<SpeedOutlinedIcon />} />
            <BottomNavigationAction label="Account" icon={<PersonOutlineIcon />} />
        </BottomNavigation>
    )
}

const ROUTE_OPTIONS = {
    0: VENDOR_URL+ORDERS_URL,
    1: VENDOR_URL+MENU_URL,
    2: VENDOR_URL+BUSINESS_URL,
    3: VENDOR_URL+CHEF_ACCOUNT_URL,
}

export default ChefNavigationBar;