// Chef URLs
export const ORDERS_URL = "/orders";
export const MENU_URL = "/menu";
export const ADD_DISH_URL = "/add-new-dish";
export const BUSINESS_URL = "/business";
export const CHEF_ACCOUNT_URL = "/account";
export const VIEW_DISH = "/view-dish";
export const Edit_Dish = "/edit-dish";
export const Accounts_Chef = "/account";
