import React, { useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory,
} from "react-router-dom";
import {
  ORDERS_URL,
  MENU_URL,
  BUSINESS_URL,
  CHEF_ACCOUNT_URL,
  ADD_DISH_URL,
  VIEW_DISH,
  Edit_Dish,
  Accounts_Chef,
} from "./constants/urlConstants";
import Orders from "./pages/Orders";
import Menu from "./pages/Menu";
import Business from "./pages/Business";
import ChefProfile from "./pages/ChefProfile";
import AddNewDish from "./pages/AddNewDish";
import ViewDish from "./pages/ViewDish";
import EditDish from "./pages/EditDish";
import AccountsChef from "./pages/Accounts";

import { useSelector, useDispatch } from "react-redux";

function Vendor({ match }) {
  const dishListByChef = useSelector(
    (state) => state.dishesDetails.dishListByChef
  );

  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    const data = JSON.parse(sessionStorage.getItem("userData"));
    if (!data) {
      history.push("/");
    } else {
      // TODO: call backend API to verify the id token https://developers.google.com/identity/sign-in/web/backend-auth
      dispatch.orderDetails.getOrdersList();

      if (dishListByChef.length === 0)
        // Lazy loading
        dispatch.dishesDetails.getAllDishesForChef();
    }
  }, []);
  return (
    <>
      <Route path={`${match.path}`} exact>
        <Orders />
      </Route>
      <Route path={`${match.path}${ORDERS_URL}`}>
        <Orders />
      </Route>
      <Route path={`${match.path}${MENU_URL}`}>
        <Menu />
      </Route>
      <Route path={`${match.path}${BUSINESS_URL}`}>
        <Business />
      </Route>
      <Route path={`${match.path}${CHEF_ACCOUNT_URL}`}>
        <ChefProfile />
      </Route>
      <Route path={`${match.path}${ADD_DISH_URL}`}>
        <AddNewDish />
      </Route>
      <Route path={`${match.path}${VIEW_DISH}`}>
        <ViewDish />
      </Route>
      <Route path={`${match.path}${Edit_Dish}`}>
        <EditDish />
      </Route>
      <Route path={`${match.path}${Accounts_Chef}`}>
        <AccountsChef />
      </Route>
    </>
  );
}

export default Vendor;
