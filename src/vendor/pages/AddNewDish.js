import React from "react";
import ImageUpload from "../components/ImageUpload";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import { CATEGORY_LIST } from "../config/appConfigurations";
import { Quantity_List } from "../config/appConfigurations";
import Container from "@material-ui/core/Container";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { MENU_URL } from "../constants/urlConstants";
import { VENDOR_URL } from "../../common/utils/urlConstants";

const styles = (theme) => ({
  root: {
    width: "100%",
    display: "flex",
    flexDirection: "column", // overflowX: "hidden",
  },
  textField: {
    //    marginLeft: "10%",
    // marginRight: "10%",
    //marginTop: "10%",
    // margin: "10% 0% 0% 10%",
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  switch: {
    marginLeft: "10%",
    marginRight: "10%",
    marginTop: "10%",
    // marginBottom: "-5%",
  },
  submitBtn: {
    //left: "50%",
    marginTop: "10%",
  },
});
class AddNewDish extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      itemImage: {},
      itemName: "",
      itemCategory: "",
      itemType: true,
      itemDesc: "",
      itemQuantity: "",
      itemPrice: "",
    };
  }
  onImageChange = (image) => {
    this.setState({ itemImage: image });
  };

  handleFieldChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = (e) => {
    this.props.saveNewItem(this.state);
    this.props.history.push(MENU_URL);
    this.props.history.push(VENDOR_URL + MENU_URL);
    e.preventDefault();
  };
  render() {
    const { classes } = this.props;
    const option = { label: "dsdsf", value: 33 };
    return (
      <Container>
        <form
          className={classes.root}
          noValidate
          autoComplete="off"
          onSubmit={this.handleSubmit}
        >
          <br />
          <ImageUpload onImageChange={this.onImageChange} />
          <TextField
            id="standard-basic"
            label="Item Name"
            className={classes.textField}
            fullWidth
            name="itemName"
            onChange={this.handleFieldChange}
          />
          <TextField
            className={classes.textField}
            select
            label="Category"
            fullWidth
            value={this.state.itemCategory}
            name="itemCategory"
            onChange={this.handleFieldChange}
          >
            {CATEGORY_LIST.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>

          <FormControlLabel
            className={classes.switch}
            control={
              <Switch
                checked={this.state.itemType}
                onChange={this.handleFieldChange}
                name="itemType"
                color="primary"
              />
            }
            label="Veg"
          />
          <TextField
            id="standard-basic"
            label="Description"
            className={classes.textField}
            name="itemDesc"
            onChange={this.handleFieldChange}
            fullWidth
          />
          {/* <TextField id="standard-basic" type="number" label="Quantity" className={classes.textField}
                        name="itemQuantity" onChange={this.handleFieldChange} fullWidth /> */}
          <TextField
            className={classes.textField}
            select
            label="Quantity"
            fullWidth
            value={this.state.itemQuantity}
            name="itemQuantity"
            onChange={this.handleFieldChange}
          >
            {Quantity_List.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
          <FormControl fullWidth className={classes.textField}>
            <InputLabel htmlFor="standard-adornment-amount">Price</InputLabel>
            <Input
              id="standard-adornment-amount"
              type="number"
              name="itemPrice"
              onChange={this.handleFieldChange}
              endAdornment={<InputAdornment position="end">₹</InputAdornment>}
            />
          </FormControl>
          <Button
            variant="contained"
            color="secondary"
            type="submit"
            className={classes.submitBtn}
          >
            Add Item
          </Button>
        </form>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveNewItem: dispatch.dishesDetails.saveNewItem,
  };
};

export default connect(
  null,
  mapDispatchToProps
)(withStyles(styles)(withRouter(AddNewDish)));
