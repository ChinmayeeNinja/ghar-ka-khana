import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import NavigationBar from "../components/NavigationBar";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import TabPanel from "../components/TabPanel";
import AllOrders from "../components/OrdersTabs/AllOrders";
import IncomingOrders from "../components/OrdersTabs/IncomingOrders";
import { useDispatch } from "react-redux";
import DeclinedOrders from "../components/OrdersTabs/DeclinedOrders";
import PreparingOrders from "../components/OrdersTabs/PreparingOrders";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    marginTop: "10px",
    marginBottom: "20%",
    backgroundColor: theme.palette.background.paper,
  },
  listHeader: {
    marginTop: "10px",
  },
}));

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}
export default function Orders() {
  const userProfile = useSelector((state) => state.userDetails.userProfile);
  const userType = useSelector((state) => state.userDetails.userType);
  const ordersList = useSelector((state) => state.orderDetails.ordersList);
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <>
      <div>Hey {userProfile?.givenName || "Bro"}!</div>
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            aria-label="scrollable auto tabs example"
          >
            <Tab label="All" {...a11yProps(0)} />
            <Tab label="Incoming" {...a11yProps(1)} />
            <Tab label="Preparing" {...a11yProps(2)} />
            <Tab label="Declined" {...a11yProps(3)} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <AllOrders ordersList={ordersList} classes={classes} />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <IncomingOrders ordersList={ordersList} classes={classes} />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <PreparingOrders ordersList={ordersList} classes={classes} />
        </TabPanel>
        <TabPanel value={value} index={3}>
          <DeclinedOrders ordersList={ordersList} classes={classes} />
        </TabPanel>
      </div>
      <NavigationBar userType={userType} />
    </>
  );
}
