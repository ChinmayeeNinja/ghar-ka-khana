import React from "react";
import { withStyles, CardMedia } from "@material-ui/core";
import ShareSharpIcon from "@material-ui/icons/ShareSharp";
import EditSharpIcon from "@material-ui/icons/EditSharp";
import ArrowBackIosSharpIcon from "@material-ui/icons/ArrowBackIosSharp";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import QueryBuilderSharpIcon from "@material-ui/icons/QueryBuilderSharp";
import Button from "@material-ui/core/Button";
import { MENU_URL, Edit_Dish } from "../constants/urlConstants";
// import { VENDOR_URL } from "../../common/utils/urlConstants";
import { VENDOR_URL } from "../../common/utils/urlConstants";

import Grid from "@material-ui/core/Grid";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

const styles = (theme) => ({
  media: {
    minHeight: 250,
  },
  backfonts: {
    color: "white",
    paddingTop: 25,
    paddingRight: 15,
  },
  sharefonts: {
    color: "white",
    float: "right",
    paddingTop: 25,
    paddingRight: 15,
  },
  editfonts: {
    color: "white",
    float: "right",
    paddingTop: 25,
    paddingRight: 15,
  },
  gridmain: {
    marginTop: 12,
  },
  gridsub1: {
    paddingLeft: "30px!important",
    fontWeight: "bold",
  },
  gridsub2: {
    paddingLeft: "90px!important",
    fontWeight: "bold",
  },
  veg: {
    paddingLeft: "30px!important",
    fontWeight: 100,
    paddingBottom: "20px!important",
    borderBottom: "1px dashed #908d8d",
  },
  dot: {
    color: "green",
    border: "1px solid green",
    fontSize: "8px",
    padding: 2,
  },
  timeicon: {
    fontSize: "16px",
    color: "#908d8d",
  },
  time: {
    fontSize: 14,
    fontWeight: 100,
    paddingTop: "15px!important",
  },
  category: {
    paddingLeft: "30px!important",
    fontSize: 14,
    fontWeight: 100,
    paddingTop: "15px!important",
  },
  meals: {
    paddingLeft: "30px!important",
    paddingBottom: "15px!important",
    fontWeight: 600,
  },
  mealstime: {
    paddingBottom: "15px!important",
    fontWeight: 600,
  },
  vegthalitext: {
    borderBottom: "1px dashed #908d8d",
    borderTop: "1px dashed #908d8d",
    paddingLeft: "30px!important",
    paddingTop: "15px!important",
    paddingBottom: "15px!important",
    fontWeight: 300,
    fontSize: 14,
  },
  deleteitem: {
    marginTop: 20,
  },
  buttondiv: {
    display: "flex",
    justifyContent: "flex-end",
    paddingRight: "1.5rem!important",
  },
});
class ViewDish extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    const { selectedDish, getDishDetails } = this.props;
    getDishDetails(selectedDish);
  }
  handleback = () => {
    this.props.history.goBack(MENU_URL);
  };
  handleEdit = () => {
    // alert("hi");
    this.props.history.push(VENDOR_URL + Edit_Dish);
  };
  render() {
    const { classes } = this.props;
    const { selectedDishDetails } = this.props;
    console.log(selectedDishDetails);
    return (
      <div>
        <CardMedia
          className={classes.media}
          image={selectedDishDetails?.[0]?.photo}
          title="img"
        >
          {" "}
          <ArrowBackIosSharpIcon
            className={classes.backfonts}
            onClick={this.handleback}
          />
          <EditSharpIcon
            className={classes.editfonts}
            onClick={this.handleEdit}
          />
          <ShareSharpIcon className={classes.sharefonts} />
        </CardMedia>
        <Grid container className={classes.gridmain} spacing={2}>
          <Grid item xs={6} className={classes.gridsub1}>
            {selectedDishDetails?.[0]?.name}{" "}
          </Grid>
          <Grid item xs={6} className={classes.gridsub2}>
            ₹{selectedDishDetails?.[0]?.base_price}
          </Grid>
          <Grid item xs={12} className={classes.veg}>
            <FiberManualRecordIcon className={classes.dot} />{" "}
            {selectedDishDetails?.[0]?.type}
          </Grid>
          <Grid item xs={6} className={classes.category}>
            Category
          </Grid>
          <Grid item xs={6} className={classes.time}>
            <QueryBuilderSharpIcon className={classes.timeicon} /> Timeslots
          </Grid>
          <Grid item xs={6} className={classes.meals}>
            {selectedDishDetails?.[0]?.category}
          </Grid>
          <Grid item xs={6} className={classes.mealstime}>
            10:00am,10:30am
          </Grid>
          <Grid item xs={12} className={classes.vegthalitext}>
            {selectedDishDetails?.[0]?.description}
          </Grid>
          <Grid item xs={12} className={classes.buttondiv}>
            <Button
              variant="outlined"
              color="secondary"
              className={classes.deleteitem}
            >
              Delete item
            </Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  selectedDish: state.dishesDetails.selectedDish,
  selectedDishDetails: state.dishesDetails.selectedDishDetails,
});
const mapDispatchToProps = (dispatch) => {
  return {
    // getDishDetailss: dispatch.dishesDetails.getDishDetails,
    getDishDetails: dispatch.dishesDetails.getDishDetails,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(withRouter(ViewDish)));
