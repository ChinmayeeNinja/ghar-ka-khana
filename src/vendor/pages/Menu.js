import React from "react";
import { connect, useDispatch } from "react-redux";
import NavigationBar from "../components/NavigationBar";
import { withStyles } from "@material-ui/core/styles";
import SearchBar from "material-ui-search-bar";
import NativeSelect from "@material-ui/core/NativeSelect";
import DishItem from "../components/ChefDishes/DishItem";
import { Grid } from "@material-ui/core";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Hidden from "@material-ui/core/Hidden";
import { ADD_DISH_URL, VIEW_DISH } from "../constants/urlConstants";
import { withRouter } from "react-router-dom";
import { VENDOR_URL } from "../../common/utils/urlConstants";

const styles = (theme) => ({
  root: {
    maxWidth: "90%",
    marginLeft: "5%",
    marginBottom: "20%",
  },
  media: {
    height: 140,
  },
  searchBar: {
    width: "100%",
  },
  dishGrid: {
    marginTop: "1%",
  },
  fab: {
    position: "sticky",
    bottom: "10%",
    [theme.breakpoints.down("xs")]: {
      left: "35%",
    },
    left: "40%",
  },
  fabBtn: {
    textTransform: "none",
  },
});

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activeCategory: "" };
  }
  componentDidMount() {
    const { dishesList, getAllDishesForChef } = this.props;
    if (dishesList.length == 0) getAllDishesForChef();
  }
  handleAddNewItem = () => {
    this.props.history.push(VENDOR_URL + ADD_DISH_URL);
  };
  handleViewDish = (e) => {
    this.props.selectDish(e.target.id);
    this.props.history.push(VENDOR_URL + VIEW_DISH);
  };

  render() {
    const { classes, dishesList, dishCategoryList } = this.props;
    return (
      <>
        <div className={classes.root}>
          <NativeSelect
            value={this.state.activeCategory}
            onChange={(e) => {
              this.setState({ activeCategory: e.target.value });
            }}
            inputProps={{
              name: "dishCategory",
            }}
          >
            <option value="">All Dishes</option>
            {dishCategoryList.map((category) => (
              <option value={category}>{category}</option>
            ))}
          </NativeSelect>
          <SearchBar
            onChange={() => console.log("onChange")}
            onRequestSearch={() => console.log("onRequestSearch")}
            style={{
              margin: "0 auto",
              maxWidth: 800,
            }}
          />
          <Grid container spacing={2} className={classes.dishGrid}>
            {dishesList.map((item, i) => {
              if (
                this.state.activeCategory === "" ||
                this.state.activeCategory === item.category
              )
                return (
                  <Grid item xs={6} sm={6} md={4} lg={3}>
                    <DishItem
                      key={i}
                      image_url={item.photo}
                      item_name={item.name}
                      item_price={item.final_price}
                      item_description={item.description}
                      item_status={item.status}
                      item_id={item.id}
                      onClick={this.handleViewDish}
                    />
                  </Grid>
                );
            })}
          </Grid>
          <NavigationBar userType={this.props.userType} />
        </div>
        <Grid item xs={4} sm={4} md={4} lg={4} className={classes.fab}>
          <Fab
            variant="extended"
            color="primary"
            aria-label="add"
            className={classes.fabBtn}
            onClick={this.handleAddNewItem}
          >
            <Hidden xsDown>
              <AddIcon />
            </Hidden>
            Add New Dish
          </Fab>
        </Grid>
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  userType: state.userDetails.userType,
  dishesList: state.dishesDetails.dishListByChef,
  dishCategoryList: state.dishesDetails.dishCategoryList,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getAllDishesForChef: dispatch.dishesDetails.getAllDishesForChef,
    selectDish: dispatch.dishesDetails.selectDish,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(withRouter(Menu)));
