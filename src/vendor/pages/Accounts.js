import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import { withStyles, CardMedia } from "@material-ui/core";
import EditSharpIcon from "@material-ui/icons/EditSharp";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Grid from "@material-ui/core/Grid";

const styles = (theme) => ({
  media: {
    minHeight: 70,
  },
  grid: {
    paddingLeft: 20,
    paddingTop: 20,
  },
  edit: {
    float: "right",
    paddingRight: 20,
  },
  details: {
    marginTop: 30,
    paddingLeft: 20,
  },
  detailname: {
    fontSize: 14,
    fontWeight: 300,
    color: "grey",
  },
});
class AccountsChef extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { classes } = this.props;

    return (
      <>
        <Grid container spacing={3} className={classes.grid}>
          <Grid item xs={4}>
            <CardMedia
              className={classes.media}
              image="https://b.zmtcdn.com/data/pictures/6/246/ba50a5176f9b3abf84a4b734543474a2.jpg?output-format=webp"
              title="img"
            />{" "}
          </Grid>
          <Grid item xs={4}>
            <Typography>Suraj Jagtap</Typography>
            <Typography>Suraj Jagtap</Typography>
          </Grid>
          <Grid item xs={4}>
            <EditSharpIcon className={classes.edit} />
          </Grid>
        </Grid>
        <Grid className={classes.details}>
          <Typography className={classes.detailname}>
            Area Catering To
          </Typography>
          <Typography>Kothrud </Typography>
        </Grid>
        <Grid className={classes.details}>
          <Typography className={classes.detailname}>Speciality</Typography>
          <Typography>Fish Curry,Veg Thali,Suramai Fry,Bangda Fry </Typography>
        </Grid>
        <Grid className={classes.details}>
          <Typography className={classes.detailname}>Type</Typography>
          <Typography>Veg,Non-Veg </Typography>
        </Grid>
        <Grid className={classes.details}>
          <Typography className={classes.detailname}>
            Cooking Categories
          </Typography>
          <Typography>Meal,Breakfast </Typography>
        </Grid>
        <Grid className={classes.details}>
          <Typography className={classes.detailname}>FSSI Number</Typography>
          <Typography>Meal,Breakfast </Typography>
        </Grid>
      </>
    );
  }
}
const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  selectedDish: state.dishesDetails.selectedDish,
  selectedDishDetails: state.dishesDetails.selectedDishDetails,
});
const mapDispatchToProps = (dispatch) => {
  return {
    getDishDetails: dispatch.dishesDetails.getDishDetails,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(withRouter(AccountsChef)));
