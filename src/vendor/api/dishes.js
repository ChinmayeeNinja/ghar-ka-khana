import Axios from "axios";

export const getAllDishesByChef = (chefId) => {
  return Axios.get("../sample/sample-allChefItems.json")
    .then((response) => {
      return response?.data?.data;
    })
    .catch((error) => {
      console.log(error);
    });
};
